# eaas-orgctl

The command-line tool for managing Emulation-as-a-Service's organizations/nodes.


# How to use

Currently, the tool is implemented as a single Python 3 script, using standard dependencies.
Hence it should be runnable as-is without any installation. To get an overview of all available
sub-commands and options, use the in-built documentation:

```sh
$ ./eaas-orgctl --help
$ ./eaas-orgctl <sub-command> --help
```

## Access credentials

The tool currently requires the following credentials for executing most commands:
- `endpoint`: URL where EaaS is deployed
- `user`: name of an existing _superuser_ with admin privilegues
- `password`: superuser's password

Credentials can be specified with corresponding command-line arguments or alternatively loaded
with `-c/--credentials` from a JSON file with the following content:

```json
{
   "endpoint": "<endpoint>",
   "password": "<password>",
   "user": "<username>"
}

```

Password will be requested at runtime if unset.


## Generating org-config template

To generate a template for organization's config, run the following command:
```sh
$ ./eaas-orgctl template > org.json
```

Then replace all empty placeholders with meaningful values using a text-editor.


## Creating organizations

To create a new organization from existing org-config, run the following command:
```sh
$ ./eaas-orgctl -e '<endpoint-url>' -u '<super-admin-user>' create '<org-config-file>'
```

## Listing organizations

To list all existing organizations, run the following command:
```sh
$ ./eaas-orgctl -e '<endpoint-url>' -u '<super-admin-user>' list
```

## Listing organization's members

To list all existing members of an organization, run the following command:
```sh
$ ./eaas-orgctl -e '<endpoint-url>' -u '<super-admin-user>' members '<tenant-id>'
```
